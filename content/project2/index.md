+++
title = "AWS S3 Bucket"
description = "Create an S3 Bucket using CDK with AWS CodeWhisperer."
date = 2024-03-02
updated = 2024-03-02
draft = false

[taxonomies]
tags = ["Projects"]
[extra]
keywords = "Code, Code Blocks, Syntax, Syntax Highlighting, Theme"
toc = true
+++

## Project Introduction
The main goal is to create an S3 Bucket using AWS CDK and AWS CodeWhisperer. AWS CDK will be used to generate the code for setting up the S3 Bucket, while AWS CodeWhisperer will assist in creating the corresponding codebase. In this project, it is required to include specific properties for the S3 bucket, such as versioning and encryption, to improve the functionality and security of the created bucket.

## Generated S3 Bucket Screenshots

### Generated Bucket
![Generated Bucket](./images/1.png)

### Bucket Property 1: Versioning
![Versioning](./images/2.png)

### Bucket Property 2: Encryption
![Encryption](./images/3.png)

## Project Detailed Setup
1. Log in to [CodeCatalyst](https://codecatalyst.aws/explore) and create a project named `week3-mini-project`. 
2. Create a new development environment under this project and choose **AWS Cloud9 (in browser)** as the option.
3. In the **IAM** service in the AWS Management Console, create a new user and attach the following policies: `IAMFullAccess`, `AmazonS3FullAccess`, `AWSLambda_FullAccess`.
4. After user creation, add inline policies for `CloudFormation` and `Systems Manager`. Make sure to select all access options, otherwise you may not be able to create successfully.
5. Generate an access key for the new user and save the ID and the secret key in somewhere safe.
6. Go to the Cloud9 IDE and run the following command to set up access keys and the region.
```
aws configure
# Input access key and region
```
7. Create a new directory for your project (e.g. week3-mini-project) and navigate into it.
```
mkdir week3-mini-project
cd week3-mini-project/
```
8. Use the following command to create the project template.
```
cdk init app --language=typescript
```

## CodeWhisperer Usage
1. Enable **CodeWhisperer** by clicking the AWS logo in the sidebar under Developer tools.
2. Utilize CodeWhisperer to generate code. Under the file `/lib/mini_proj3-stack.ts`, I used the following prompt to generate the S3 bucket code
```
// make an S3 bucket and enable versioning and encryption 
```

3. Under the file `/bin/mini_proj3.ts`, I used the following prompt to generate the necessary variables:
```
// add necessary variables to create the S3 bucket 
```

### Project Deployment
1. After code generation, run the following command to compile the TypeScript file.
```
npm run build
```
2. Use the following command  to create the CloudFormation template.
```
cdk synth
```
3. Deploy the template with the following commands.
```
cdk bootstrap
cdk deploy
```
4. Navigate to the AWS account and verify the S3 bucket and its properties.

## Project URL
You can access the project repo [here](https://gitlab.com/dukeaiml/IDS721/yc557-week3-mini-project).