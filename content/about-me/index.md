+++
title = "About Me"
description = "My name is Yushun (Oliver) Chen."
date = 2024-01-29
updated = 2024-01-29
draft = false

[taxonomies]
tags = ["Me"]
[extra]
toc = true
series = "Features"
+++

<!-- more -->

<div align="center">
  <img src="profile.JPG" alt="profile" width="300" height="200">
</div>

My name is Yushun (Oliver) Chen. I am currently a Master's Student in Electrical and Computer Engineering with a concentration in software development at Duke University. I have graduated from UW-Madison in May 2022. I triple-majored in B.S. Computer Sciences, Statistics, and Applied Mathematics. I was also in the Letter & Sciences Honors Program. Here is my resume and here is my course guide of all the courses that I have taken. For future research interests, I am particularly interested in software engineering and HCI.
