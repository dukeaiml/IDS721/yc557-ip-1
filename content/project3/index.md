+++
title = "Rust Actix Web Application with Docker"
description = "Containerizing a simple Rust Actix web application using Docker."
date = 2024-03-03
updated = 2024-03-03
draft = false

[taxonomies]
tags = ["Projects"]
[extra]
keywords = "Code, Code Blocks, Syntax, Syntax Highlighting, Theme"
toc = true
+++

## Project Introduction
This project involves containerizing a simple Rust Actix web application using Docker. The goal is to create a self-contained environment for the web service, ensuring easy deployment and scalability. The project emphasizes efficient container functionality, a well-constructed Dockerfile, and clear documentation.

## Detailed Setup
1. Install Docker from its official website [here](https://docs.docker.com/desktop/install/mac-install/).
2. Run the following command to check if Docker is successfully installed:
```
sudo docker run hello-world
```
3. Create a Actix-web application based on its official documentation [here](https://actix.rs/docs/getting-started/).
```
cargo new week4-mini-project
cd week4-mini-project
```
4. Add the following dependency to `Cargo.toml` file:
```
[dependencies]
actix-web = "4"
```
5. Write a simple application in `main.rs`. This program makes a little website using Rust and Actix-Web. Each time you refresh the page, it shows a different cool message from a list.
6. Test by running the application locally first.
```
cargo run
```

## Docker Usage
1. Write the Dockerfile as following:
```
# Use the official Rust 1.75 image as the base image for the builder stage
FROM rust:1.75 AS builder

# Set the working directory inside the container
WORKDIR /usr/src/week4-mini-project

# Change the user to root for copying files (optional, depending on your needs)
USER root

# Copy the entire content of the local directory into the container's working directory
COPY . .

# Build the project using Cargo with the --release flag for optimized code
RUN cargo build --release

# Expose port 8080 to the outside world (used by Actix-Web)
EXPOSE 8080

# Define the default command to run when the container starts, using 'cargo run'
CMD cargo run
```

2. Build the Docker image by running the following command:
```
sudo docker build -t week4-mini-project .
```

3. Run the following command to run the Docker container locally:
```
sudo docker run -p 8080:8080 week4-mini-project
```

## Demo
### Termial Outputs
Here are the outputs after running the Docker commands.
![terminal-1](./images/terminal-1.png)
![terminal-2](./images/terminal-2.png)

### Docker Container
Here is the created Docker container viewed from Docker Desktop.
![docker-desktop](./images/docker-desktop.png)

### Website Screenshot
![website](./images/website.png)

### Website Demo
Please find the demo video [here](https://gitlab.com/dukeaiml/IDS721/yc557-week4-mini-project/-/blob/main/readme-images/demo.mp4?ref_type=heads).

## Project URL
You can access the project repo [here](https://gitlab.com/dukeaiml/IDS721/yc557-week4-mini-project).