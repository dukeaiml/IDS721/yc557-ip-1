# Individual Project 1 [![pipeline status](https://gitlab.com/dukeaiml/IDS721/yc557-ip-1/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/yc557-ip-1/-/commits/main)

> Author: Oliver Chen (yc557)

> Website Link: https://ids.yushunchen.com

## Website Functionality

My portfolio website hosts all the current projects that I have worked on in IDS 721. It is built using [Zola](https://www.getzola.org/documentation/getting-started/overview/).

### Installation Steps
It is very easy to get started with Zola. Here are some basic steps:
```bash
brew install zola
zola init portfolio
```
I used the [abridge](https://www.getzola.org/themes/abridge/) theme as shown on the Zola official website. Here are the key steps:
```bash
git clone https://github.com/jieiku/abridge.git themes/abridge
rsync -r themes/abridge/content .
zola serve
```

### Functionalities
The website has many functionalities for you to explore. I will highlight some here.
1. Here is the home page for my portfolio website. It contains all the projects that I have worked on in IDS 721.
![Homepage](./readme-images/homepage.png)

2. Here is the about page to introduce myself to the audience.
![About](./readme-images/about.png)

3. For each project, it has detailed description. Here are some examples:
![Project Details 1](./readme-images/project-details.png)
![Project Details 2](./readme-images/project-details-2.png)

4. It also has darkmode for anyone interested.
![Dark mode](./readme-images/darkmode.png)

5. The projects can be organized by date/time and tags. Here is an example where they are sorted by time.
![Sort](./readme-images/projects.png)


## GitLab Workflow
1. Add the `.gitlab-ci.yml` file below to the top level file directory.
```yml
stages:
  - deploy

default:
  image: debian:stable-slim
  tags:
    - docker

variables:
  # The runner will be able to pull your Zola theme when the strategy is
  # set to "recursive".
  GIT_SUBMODULE_STRATEGY: "recursive"

  # If you don't set a version here, your site will be built with the latest
  # version of Zola available in GitHub releases.
  # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: ""

pages:
  stage: deploy
  script:
    - |
      apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
      if [ $ZOLA_VERSION ]; then
        zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
        if ! wget --quiet --spider $zola_url; then
          echo "A Zola release with the specified version could not be found.";
          exit 1;
        fi
      else
        github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
        zola_url=$(
          wget --output-document - $github_api_url |
          grep "browser_download_url.*linux-gnu.tar.gz" |
          cut --delimiter : --fields 2,3 |
          tr --delete "\" "
        )
      fi
      wget $zola_url
      tar -xzf *.tar.gz
      ./zola build

  artifacts:
    paths:
      # This is the directory whose contents will be deployed to the GitLab Pages
      # server.
      # GitLab Pages expects a directory with this name by default.
      - public

  rules:
    # This rule makes it so that your website is published and updated only when
    # you push to the default branch of your repository (e.g. "master" or "main").
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
2. Then we can check if the CI/CD pipeline passes or failes each time we push to the repository. You can access the pipelines [here](https://gitlab.com/dukeaiml/IDS721/yc557-ip-1/-/pipelines).
![Pipelines](./readme-images/pipeline.png)

## Hosting and Deployment
The website is hosted on GitLab pages. On top of that, I used a custom subdomain on it, so it can be accessed using the following URL:

> **https://ids.yushunchen.com**

The root domain `yushunchen.com` is my personal domain. You can also access my personal website using this URL: [yushunchen.com](https://yushunchen.com/), which is my side project (not this individual project 1).

![Pages](./readme-images/pages.png)

To set up custom domain, follow this official [documentation](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/) from GitLab. Here are the key steps:

1. Set up a custom domain on GitLab
![Domain](./readme-images/domain-1.png)
2. Add the following DNS records in your domain’s server control panel.
![Domain 2](./readme-images/domain-2.png)

## Demo Video
You can access a demo video of the website [here](https://gitlab.com/dukeaiml/IDS721/yc557-ip-1/-/blob/main/readme-images/demo.mp4?ref_type=heads).